# Hello World Application
Input the following into a main.go file:

        package main

        import "fmt"

        func main() {
            fmt.Println("Hello World!")
        }
_Let's break down what all the bits and pieces are!_

---

## How do we run the code in our project?

1. Open Terminal and navigate to the directory containing the `main.go` file. In my case: `cd ~/Dev/golang/`
2. In the terminal:

   - Type "`go`" which is a portal into all things go. This command will give us the ability to compile and execute all things GoLang related. 
   - Type "`run`, this will tell the go runtime to excecute and compile what ever file you tell it to next.
   - Type "`main.go`", this is the file we want to execute. Remember that all GoLang files end with `.go` at the end.
   - Press return and the file should execute and compile.

The output should look like this:

            Dev/golang/3/ % go run main.go
            > Hello World!

_Pretty Simple huh?_

![](https://media.giphy.com/media/OCu7zWojqFA1W/giphy.gif)

Let's look at some more of the things the `go` command can do.

---
## What are some other Go Commands out of the box?

- `go build` - Compiles a bunch of go source code files. *Does not execute.*
>If you run `go build main.go` nothing will output into your console but if you `ls` that directory, you will see a new file called `main` and you can now execute the file using `./main`
- `go run` - Complies and executes one or two files, note that it's similar to build but also executes.
- `go fmt` - Formats all the code in each file in the current directory
- `go test` - Runs any tests associated with the current project

The next two commands are used to handle dependencies in our projects
- `go get` - Downloads the raw source code of someone else's package
- `go install` - Compiles and "installs" a package


---

## What does the 'package main' mean?


            Package == Project == Workspace

>Packages can have many related go files and dependencies contained inside of it, the only requirement for a package is that every file have to state at the top what package they relate to. 

Inside of go there is 2 types of packages.
1. Executable Packages - Generates a file that we can run (see the go build explainer from the previous section)
2. Reusable Packages - Code used as helpers. Great Place to put reusable logic.

---

## How do we know when we are making an Executable or Reusable Package?

- It's a bit tricky, we called the first line of the file `main` which will auto create an executable package called main, anything other package name will not. One requirement of packages named "main" is that we define a function named "main" inside it, which is ran automatically when the program runs.
- `main` is a reserved package name in go. 
- If you changed the package name to anything else and then ran `go build main.go`, you wouldn't create an executable file.
---

## What does 'import "fmt"' mean?

The import statement is used to give our package (the one we are writing) access to code written inside another package.
- In order for your package to have access to any code outside of it, you have to use import statements. 
- You are not limited to packages located in the standard library, you could create your own reusable package.
- `fmt` is a standard package, you can locate "go standard packages" from https://golang.org/pkg/, this one in particular is here: https://golang.org/pkg/fmt/. This package is usually used to print out things to your terminal. 
- Standard packages are created or adopted by the core Go team and are vetted, tested, and trustworthy. These can be used to expand the tooling available in go and they have very verbose documentation, which we will lean heavily on.
---

## What is func?
`func` is short for "function" which in Go act just like other languages.  

We declare a function with the reserved keyword `func` then the name of the function then an argument list.

![https://share.drewlearns.com/5zuApq0R](https://p289.p2.n0.cdn.getcloudapp.com/items/5zuApq0R/c5730fca-b9e5-4631-8bc4-6c4cd6b323da.jpg?v=3a194afb06abec5624ec8b49318e6f64)

----

## How is the main.go file organized?
We will always organize our main.go file as follows:

        1. package main  -- package declaration
        2. import "fmt" -- import other packages
        3. func main(){fmt.PrintIn("Hello World!")} -- Tell go to do something