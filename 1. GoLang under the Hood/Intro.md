# Golang 
## What is GoLang?
- Golang, also known as just "Go" is a simple to read, easy to learn, simple(r) programing that is worth taking a look into. It's opinionated, and 
- It's an engineered language instead of an evolved language like Javascript.
- Statically typed, Compiled Language that feels Dynamically typed and interpreted.
- There is a standard library, something javascript lacks.
- Automatic memory management including garbage collection!
- Executables created with Go toolchain has no default external dependencies.
- It's interoperability is pretty neat. In Docker, Go interfaces with low-level Linux functions, cgroups, and namespaces in magical ways!
---
## What are the best applications of Go?
- Utilities and Stand Alone Tools
- Distributed networked services.
- Cloud Native development
- Most Go applications are command-line (CLI) tools or network services. Here is an example of one: https://github.com/akamai/cli
---

## _A tutorial guide by Drew K_

Follow this Repo to learn GoLang with me!

## Why GoLang?
I'm personally learning golang to improve my cloud skillset and go seems like a natural progression.  Also, have you ever wondered how that CLI you are using works? If it's recent, it may have been written in Go. So let's learn a new tool for our toolbox and jump right in!

---
## Is there a Demand for it?

If you are interested in working with cloud architecture, then Go is certainly in high demand. Should you learn go as the first step in your developer journey? Probably not. 

You may have heard that having a specific coding language matters when getting started and I totally agree and for that reason I'd recommend something like Javascript with React as a first step. Often times developers are "_ranked and filed_" on the languages they have learned. 

In any case, I'll be writing this tutorial as if you have never read or written a single line of code.

---
## Will Go help you get a job?

Learning any coding language will improve your job outlook but I don't think that learning Go will be the best one to learn to improve your job outlook first. What I mean is, I'd pick: C, Python, Java, C++, C#, Javascript, or PHP as a first language as they are more widely adopted, established and have more jobs by volume than Go. 

Having said that, Go will be a great skillset to lean on after you have conquered at least one language and within a very short period of time of learning it's patterns, you'll be productive in no time. I'd definitely recommend it!

It's failry popular, easy to learn and read. It's well documented and backed by some big players in the tech world. Take a look at it's popularity:

![https://share.drewlearns.com/bLug6y5A](https://p289.p2.n0.cdn.getcloudapp.com/items/bLug6y5A/5791352e-e6bf-4168-9cf7-8e54499bfa50.jpg?v=fa30130ca24463327cd579e5a56b0b68)
GoLang's popularity is ranked #14 just behind Swift and ahead of Ruby and has maintained [it's ranking](https://www.tiobe.com/tiobe-index/). 
>GoLang's benefits developers in multiple ways, including garbage collection, native concurrency, and many other native capabilities that reduce the need for developers to write code to handle memory leaks or networked apps.
-- [source](https://opensource.com/article/17/11/why-go-grows#:~:text=The%20Go%20programming%20language%2C%20sometimes,making%20strong%20gains%20in%20popularity.&text=Go's%20increasing%20use%20is%20due,suited%20for%20today's%20microservices%20architectures)

Being Backed by Google, I don't see this language disappearing.

---