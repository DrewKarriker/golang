package main

import "fmt"

// CREATE A NEW TYPE OF 'DECK'
// WHICH IS A SLICE OF STRINGS

type deck []string

func (props deck) print() {
	for i, card := range props {
		fmt.Println(i, card)
	}
}
