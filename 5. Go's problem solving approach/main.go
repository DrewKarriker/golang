package main

func main() {

	cards := deck{"Ace of Clubs", myCard()}
	cards = append(cards, "King of Hearts")

	cards.print()
}

func myCard() string {
	return "Ace of Diamonds"
}
