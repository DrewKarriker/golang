Google’s Go (GoLang) Intro

## How to set up your development environment

### Installation steps 
1. Install Brew (skip if you already have brew installed). 

    Run:

        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”

2. Update & Install Go, Git, SSH connection and more!
        
    brew update && brew install golang && brew install git 

    Make sure it's installed by running `go version` &  `git --version`

3. Log into your repository tool of choice, I’ll be using gitlab, and create a new project. If you don’t have one already, I highly recommend [Gitlab](https://gitlab.com)! Let’s call it GoLang and initialize it with a README.Md.  Did I mention Gitlab?
4. Connect to your GoLang Project using SSH & Git to gitlab.
    >The book Practical Cryptography With Go suggests that ED25519 keys are more secure and performant than RSA keys.
    1. Set up SSH on your account, open terminal and run 

            sh-keygen -t ed25519 -C "drewlearns@hey.com”
    
        _Feel free to change the email to your own 😉._
        >The -C flag, with a quoted comment, such as an email address, is an optional way to label your SSH keys.
    2. You’ll see a response similar to: 
    ```
    Generating public/private ed25519 key pair. Enter file in which to save the key (/home/user/.ssh/id_ed25519):
    ```
    3. Paste in from the prompt the path `/Users/home/.ssh/id_ed25519` (_for example_) and then press return.
    4. Enter a passphrase 2 times and press return. You just created secure ED25519 SSH keys for gitlab! 
    
        Now we just have to get those keys into gitlab so we can clone the repo.
    5. Run: 

            pbcopy < ~/.ssh/id_ed25519.pub

    6. Navigate to https://gitlab.com & select your avatar in the upper right corner, and click Settings > SSH Keys.
    7. Paste the public key from step 5 above into the Key text box. _Make sure your key includes a descriptive name in the Title text box_
    8. Click the Add key button.
    9. Clone the repository:![https://share.drewlearns.com/rRukq5zR](https://p289.p2.n0.cdn.getcloudapp.com/items/rRukq5zR/b625f912-4c32-42c9-8b7a-cf1b1b937a92.jpg?v=c524415bb7249e648f5475e1262d188d)
    10. Create a development directory then clone your new gitlab repository into it. You'll use the copied SSH path from above into this command. Run 
                
            mkdir ~/Dev/ && cd ~/Dev/ && git clone git@gitlab.com:DrewKarriker/golang.git && cd ~/Dev/golang

        _It should look something like this:_

            home@Computer Dev % git clone git@gitlab.com:DrewKarriker/golang.gitCloning into 'golang'...The authenticity of host 'gitlab.com (2606:4700:90:0:f22e:fbec:5bed:a9b9)' can't be established.ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.Are you sure you want to continue connecting (yes/no/[fingerprint])? yesWarning: Permanently added 'gitlab.com,2606:4700:90:0:f22e:fbec:5bed:a9b9' (ECDSA) to the list of known hosts.Enter passphrase for key '/Users/home/.ssh/id_ed25519': 
            
        _Bear in mind, you may have to enter your passphrase if applicable_

 5. Install VSCode
       - Navigate in your browser to: https://code.visualstudio.com/ and download Visual Studio Code for your operating system and install it.
       - Set up VSCode for Go:
       ![https://share.drewlearns.com/yAu68zp1](https://p289.p2.n0.cdn.getcloudapp.com/items/yAu68zp1/a8c9ac65-144c-4e35-bdcd-3bfaf66c4020.jpg?v=052c0b1d868dabd22244ce60885b2ada)
       - Set up code command to easily open a project directory from terminal into VSCode. While the VSCode window is in focus, open the command pallet by pressing the `Command` + `Shift` + `p` buttons. You will see a drop down appear at the top with a `>`. 
       - Type in `shell` and select the first suggestion. 
       ![https://share.drewlearns.com/X6u9WjDz](https://p289.p2.n0.cdn.getcloudapp.com/items/X6u9WjDz/5d10d68e-c380-47bf-9ef7-9d038f03ec34.jpg?v=4c1c6637699a1a025798122e8c207bc7). 
6. Close VSCode so the extention can install properly.
7. Open your newly downloaded project in VSCode using the Command Line. Run:

        code /dev
    or if you `cd` into the directory you want to open, you can run 

            code .
    to open all files within it.
8. Create a new file by typing `Command` + `N` 
9. At the bottom right of the screen where it says "Plain Text", click there.
![https://share.drewlearns.com/qGuXA4R0](https://p289.p2.n0.cdn.getcloudapp.com/items/qGuXA4R0/5499a839-5315-42ab-9307-e4c0b9edf330.jpg?v=203a42cf8d7ea64368edfa6b57df682a)
10. Install the Recommended extensions for Go.
![https://share.drewlearns.com/YEuXN6kj](https://p289.p2.n0.cdn.getcloudapp.com/items/YEuXN6kj/1079e93d-bebc-4913-87ec-93959a91b2ef.jpg?v=b68494d363db02fa82a88bd14623cfa5)

## Next
Now we are all set up to get started working with Go!

Next we will be working on our first very tiny program written in Go! 

See you in the next section!