package main

import "fmt"

func main() {

	cards := []string{"Ace of Clubs", myCard()}
	cards = append(cards, "King of Hearts")

	for i, card := range cards {
		fmt.Println(i, card)
	}
}

func myCard() string {
	return "Ace of Diamonds"
}
